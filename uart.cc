#include "uart.hh"

#define BAUD 38400

#include <avr/io.h>
#include <util/setbaud.h>

UART::UART()
{
    // set speed
    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA |= _BV(U2X);
#else
    UCSRA &= ~(_BV(U2X));
#endif
    UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0); // 8-bit data
    UCSRB = _BV(RXEN) | _BV(TXEN);  // enable RX and TX
}

void
UART::send(char c) const
{
    while ( !(UCSRA & _BV(UDRE)) );  // wait until last byte is written
    UDR = c;
}

void
UART::send(const char* s) const
{
    for (auto ptr = const_cast<char*>(s); *ptr; ++ptr)
        send(*ptr);
}

char
UART::read() const
{
    while(!((UCSRA) & (1<<RXC)));    // wait while data is being received
    return UDR;
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
