#ifndef UART_HH_
#define UART_HH_

class UART {
public:
    UART();

    void send(char c) const;
    void send(const char* s) const;
    char read() const;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
