#include "addressline.hh"

#include <avr/io.h>

AddressLine::AddressLine()
{
    DDRA = 0xFF;           // first 8 bits
    DDRB |= _BV(PORTB3);   // PB3 in ATMEGA connected to RCLK in 74HC595
    PORTB |= _BV(PORTB3);
}

void
AddressLine::set(uint16_t data)
{
    // set first 8 bits
    PORTA = data & 0xFF;

    // set last 8 bits
    spi.send(data >> 8);
    PORTB &= ~_BV(PORTB3);
    PORTB |= _BV(PORTB3);
    spi.select_slave(6);   // Y6 in 74HC138 connected to OE in 74HC595
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
