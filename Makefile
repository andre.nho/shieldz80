#
# config
#

PROJECT    = shieldz80
OBJS       = main.o spi.o #addressline.o uart.o spi.o
DEBUG      = 0

MC         = atmega16
MC_SPEED   = 16000000
# PROGRAMMER = pi_1
PROGRAMMER = avrisp
EXTRA      = -b 19200 -P /dev/ttyACM0
HFUSE      = 0xD9
EFUSE      = 0xFF
LFUSE      = 0xFF

#
# variables
#

CC      = avr-gcc
CXX     = avr-g++
ATMEL   = -mcall-prologues -mmcu=$(MC) -DF_CPU=$(MC_SPEED) -DDEBUG=$(DEBUG)
CXXFLAGS  = -std=c++11 -Wall -Wextra -Os $(ATMEL)
# CFLAGS  = -std=c11 -Wall -Wextra -g $(ATMEL)
LDFLAGS = $(ATMEL)

# 
# rules
#

all: $(PROJECT).hex

$(PROJECT).elf: $(OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(PROJECT).hex: $(PROJECT).elf
	avr-objcopy -j .text -j .data -O ihex $^ $@

test-connection:
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA)

upload: $(PROJECT).hex
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA) -U flash:w:$^

listen:
	screen /dev/ttyUSB0 38400   # press Ctrl-A '\' to kill

fuse:
	avrdude -p $(MC) -c $(PROGRAMMER) $(EXTRA) -U lfuse:w:$(LFUSE):m -U hfuse:w:$(HFUSE):m #-U efuse:w:$(EFUSE):m

clean:
	rm -f *.o *.elf *.hex

.PHONY: upload clean
