#ifndef SPI_HH_
#define SPI_HH_

#include <stdint.h>

class SPI {
public:
    SPI();

    void select_slave(uint8_t n) const;
    void send(uint8_t data) const;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
