#ifndef ADDRESSLINE_HH_
#define ADDRESSLINE_HH_

#include "spi.hh"

extern SPI spi;

class AddressLine {
public:
    AddressLine();

    void set(uint16_t data);
    void high_impedance();
};


#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
