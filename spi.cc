#include "spi.hh"

#include <avr/io.h>

SPI::SPI()
{
    // slave select ports (connected to 74HC138)
    DDRB |= _BV(PORTB0) | _BV(PORTB1) | _BV(PORTB2);
    select_slave(0);

    // AVR SPI ports
    DDRB |= _BV(PORTB5) | _BV(PORTB7);  // MOSI, SCK
    // TODO - MISO

    // initialize SPI
    SPCR = _BV(SPE)      // enable SPI
         | _BV(MSTR)     // the uC is master
         | _BV(SPR0) | _BV(SPR1);

    send(0);
}

void
SPI::select_slave(uint8_t n) const
{
    PORTB = (PORTB & 0b11111000) | (n & 0b111);
}

void
SPI::send(uint8_t data) const
{
    SPDR = data;
    while(!(SPSR & (1<<SPIF))) ;
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
